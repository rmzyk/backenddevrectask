﻿namespace BackendDevRecTask.DataMsSql.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core.Domain;
    using Core.Repositories;
    using EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Infrastructure.API.IRepositories;
    using AutoMapper;
    using BackendDevRecTask.DataMsSql.DAO;
    using System;

    public class CompanyRepository : ICompanyRepository, IMsSqlRepository
    {
        private readonly BackendDevRecTaskContext _dbContext;
        private readonly IMapper _mapper;

        public CompanyRepository(BackendDevRecTaskContext dbContext,
            IMapper mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Company>> GetByFilterAsync()
            => _mapper.Map<IEnumerable<Company>>(await _dbContext.Company.ToListAsync());

        public async Task UpdateAsync(Company company)
        {
            var oldCompany = await _dbContext.Company.FindAsync(company.CompanyId);
            _dbContext.Entry(oldCompany).CurrentValues.SetValues(company);

            foreach (var employee in company.Employees)
            {
                var oldEmployee = await _dbContext.Employee.FindAsync(employee.EmployeeId);

                if (!(oldEmployee is null)) continue;
                employee.SetCompany(company);
                await _dbContext.AddRangeAsync(employee);
            }

            await _dbContext.SaveChangesAsync();
        }  

        public async Task DeleteAsync(Company company)
        {
            _dbContext.Company.Remove(_mapper.Map<CompanyEntity>(company));
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Company> GetByIdAsync(Guid companyId)
            => _mapper.Map<Company>(await _dbContext.Company.Include(company => company.Employees)
                .FirstOrDefaultAsync(company => company.Id == companyId));

        public async Task<Company> GetByNameAsync(string companyName)
            => _mapper.Map<Company>(await _dbContext.Company.Include(company => company.Employees)
                .FirstOrDefaultAsync(x => x.Name == companyName));

        public async Task CreateAsync(Company company)
        {
            await _dbContext.Company.AddAsync(_mapper.Map<CompanyEntity>(company));
            await _dbContext.SaveChangesAsync();
        }
    }
}
