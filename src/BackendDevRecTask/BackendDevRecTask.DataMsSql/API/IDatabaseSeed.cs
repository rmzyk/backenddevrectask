﻿namespace BackendDevRecTask.DataMsSql.API
{
    using System.Threading.Tasks;

    public interface IDatabaseSeed : IMigration
    {
        Task SeedAsync();
    }
}
