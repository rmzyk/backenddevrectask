﻿namespace BackendDevRecTask.DataMsSql.EntityFrameworkCore
{
    using Microsoft.EntityFrameworkCore;
    using BackendDevRecTask.DataMsSql.DAO;

    public class BackendDevRecTaskContext : DbContext
    {
        private readonly MsSqlSettings _msSqlSettings;

        public BackendDevRecTaskContext(DbContextOptions<BackendDevRecTaskContext> options,
            MsSqlSettings msSqlSettings)
            : base(options)
        {
            _msSqlSettings = msSqlSettings;
        }

        public DbSet<CompanyEntity> Company { get; set; }

        public DbSet<EmployeeEntity> Employee { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_msSqlSettings.ConnectionString);
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.EnableDetailedErrors();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var companyBuilder = modelBuilder.Entity<CompanyEntity>();
            companyBuilder.HasKey(x => x.Id);
            companyBuilder.HasMany(x => x.Employees)
                          .WithOne(x => x.Company);

            var employeeBuilder = modelBuilder.Entity<EmployeeEntity>();
                employeeBuilder.HasKey(x => x.Id);
                employeeBuilder.HasOne(x => x.Company)
                    .WithMany(x => x.Employees);

            employeeBuilder.Property(x => x.JobTitle)
                .HasColumnType("nvarchar(24)");

            base.OnModelCreating(modelBuilder);
        }

    }
}
