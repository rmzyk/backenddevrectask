﻿using System;
using System.Collections.Generic;

namespace BackendDevRecTask.DataMsSql.DAO
{
    public class CompanyEntity
    {
        public Guid Id { get; set; }

        public string Name { get;  set; }

        public short EstablishmentYear { get;  set; }

        public virtual IEnumerable<EmployeeEntity> Employees { get; private set; }
    }
}
