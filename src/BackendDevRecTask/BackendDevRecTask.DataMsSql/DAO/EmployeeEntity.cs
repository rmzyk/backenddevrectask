﻿namespace BackendDevRecTask.DataMsSql.DAO
{
    using BackendDevRecTask.Core.Utils;
    using System;

    public class EmployeeEntity
    {
        public Guid Id { get; set; }

        public CompanyEntity Company { get; set; }

        public string FirstName { get;  set; }

        public string LastName { get;  set; }

        public DateTime DateOfBirth { get;  set; }

        public JobTitle JobTitle { get;  set; }
    }
}
