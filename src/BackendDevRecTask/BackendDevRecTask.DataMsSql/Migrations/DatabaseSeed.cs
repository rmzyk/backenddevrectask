﻿namespace BackendDevRecTask.DataMsSql.Migrations
{
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using API;
    using Core.Domain;
    using EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    public class DatabaseSeed : IDatabaseSeed
    {
        private readonly BackendDevRecTaskContext _context;

        public DatabaseSeed(BackendDevRecTaskContext context)
        {
            _context = context;
        }


        public async Task SeedAsync()
        {
            var isDbEmpty = await _context.Company.FirstOrDefaultAsync();
            if (isDbEmpty is null)
            {
                await _context.AddRangeAsync(GetDummyCompanyData());
                await _context.SaveChangesAsync();
            }
        }

        private static IEnumerable<Company> GetDummyCompanyData()
        {
            var listOfCompanies = new List<Company>();
            for (var i = 0; i < 100; i++)
            {
                var company = new Company( $"company{i}", (byte)(i + 1970), new List<Employee>());
                listOfCompanies.Add(company);
            }
            return listOfCompanies;
        }
    }
}
