﻿namespace BackendDevRecTask.Api.Framework
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using DataMsSql.EntityFrameworkCore;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
    using ZNetCS.AspNetCore.Authentication.Basic;
    using ZNetCS.AspNetCore.Authentication.Basic.Events;

    public static class ServiceSettings
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(
                     options =>
                     {
                         options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                         options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                         options.SerializerSettings.Formatting = Formatting.Indented;
                     })
                .AddHybridModelBinder();

            services.AddEntityFrameworkSqlServer()
                .AddDbContext<BackendDevRecTaskContext>();

            services.AddApiVersioning(_ =>
            {
                _.ReportApiVersions = true;
                _.UseApiBehavior = false;
            });

            services
                .AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
                .AddBasicAuthentication(
                    options =>
                    {
                        options.Realm = "My Application";
                        options.AjaxRequestOptions.SuppressWwwAuthenticateHeader = true;
                        options.Events = new BasicAuthenticationEvents
                        {
                            OnValidatePrincipal = context =>
                            {
                                if ((context.UserName.ToLower() != "user") || (context.Password != "secret"))
                                    return Task.FromResult(AuthenticateResult.Fail("Authentication failed."));

                                var claims = new List<Claim>
                                {
                                    new Claim(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer)
                                };

                                var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, BasicAuthenticationDefaults.AuthenticationScheme));
                                context.Principal = principal;

                                return Task.CompletedTask;
                            }
                        };
                    });
            
            return services;
        }
    }
}
