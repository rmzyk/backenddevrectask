﻿namespace BackendDevRecTask.Api.Controllers
{
    using Infrastructure.API.ICommands;

    public class EmployeeController : CustomControllerBase
    {
        public EmployeeController(ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
        }
    }
}
