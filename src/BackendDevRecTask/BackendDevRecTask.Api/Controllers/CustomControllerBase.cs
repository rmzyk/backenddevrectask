﻿namespace BackendDevRecTask.Api.Controllers
{
    using System.Threading.Tasks;
    using Infrastructure.API.ICommands;
    using Microsoft.AspNetCore.Mvc;

    [Route("[controller]")]
    public abstract class CustomControllerBase : Controller
    {
        private readonly ICommandDispatcher CommandDispatcher;

        protected CustomControllerBase(ICommandDispatcher commandDispatcher)
        {
            CommandDispatcher = commandDispatcher;
        }

        protected async Task DispatchAsync<T>(T command) where T : ICommand
            => await CommandDispatcher.DispatchAsync(command);
    }
}
