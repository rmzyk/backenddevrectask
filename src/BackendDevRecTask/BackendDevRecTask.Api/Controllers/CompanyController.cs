﻿using HybridModelBinding;

namespace BackendDevRecTask.Api.Controllers
{
    using System;
    using Infrastructure.DTO;
    using Microsoft.AspNetCore.Authorization;
    using System.Threading.Tasks;
    using Infrastructure.API.ICommands;
    using Infrastructure.API.IServices;
    using Infrastructure.Commands.Company;
    using Microsoft.AspNetCore.Mvc;

    
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CompanyController : CustomControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICommandDispatcher commandDispatcher, ICompanyService companyService)
            : base(commandDispatcher)
        {
            _companyService = companyService;
        }

        [Authorize]
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Post([FromBody]CreateCompany command)
        {
            await DispatchAsync(command);
            return Created(String.Empty ,new {Id = command.CompanyId});
        }

        [Authorize]
        [HttpPut]
        [Route("update/{CompanyId}")]
        public async Task<IActionResult> Put([FromHybrid]UpdateCompany command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [Authorize]
        [HttpDelete]
        [Route("delete/{command.CompanyId}")]
        public async Task<IActionResult> Delete([FromRoute]DeleteCompany command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Get([FromQuery] BasicFilterDto filterDto)
        {
            return Json( await _companyService.GetByFilterAsync(filterDto));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var company = await _companyService.GetByIdAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            return Json(company);
        }
    }
}
