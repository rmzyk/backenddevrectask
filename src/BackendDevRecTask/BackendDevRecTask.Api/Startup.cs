﻿using BackendDevRecTask.Api.Middlewares;

namespace BackendDevRecTask.Api
{
    using System;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using Framework;
    using Infrastructure.IoC.Modules;
    using Microsoft.AspNetCore.Builder;
    using DataMsSql.API;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IContainer AppContainer { get; private set; }  
        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.ConfigureServices();

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule(new ContainerModule(Configuration));
            AppContainer = builder.Build();

            return new AutofacServiceProvider(AppContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifetime)
        {
            var seed = app.ApplicationServices.GetRequiredService<IDatabaseSeed>();
            seed.SeedAsync();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            { 
                app.UseHsts();
            }
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseMvc();
            applicationLifetime.ApplicationStopped.Register(() => AppContainer.Dispose());
        }
    }
}
