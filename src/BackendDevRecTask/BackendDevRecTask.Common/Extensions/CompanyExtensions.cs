﻿using System;
using System.Collections.Generic;
using System.Text;
using BackendDevRecTask.Core.Domain;

namespace BackendDevRecTask.Common.Extensions
{
    public static class CompanyExtensions
    {

        public static bool Exists(this Company company)
            => company is null;
    }
}
