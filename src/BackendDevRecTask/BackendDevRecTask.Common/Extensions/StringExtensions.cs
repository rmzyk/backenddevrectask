﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendDevRecTask.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsEmptyOrWhiteSpace(this string text)
            => text == "" || text == " ";


    }
}
