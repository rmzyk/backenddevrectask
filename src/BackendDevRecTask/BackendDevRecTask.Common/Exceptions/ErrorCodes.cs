﻿namespace BackendDevRecTask.Common.Exceptions
{
    public static class ErrorCodes
    {
        public static string ErrorCodeCompanyNameInUse => "company_name_in_use";

        public static string ErrorCodeIncompleteDataProvided => "incomplete_data_provided";

        public static string ErrorCodeCompanyNotFound => "company_not_found";

        public static string ErrorCodeThereIsNoDataSatisfyingRequestedConditions => "there_is_no_data_satisfying_requested_conditions";

        public static string ErrorCodeInvalidCompanyNameFormat => "invalid_company_name_format";

        public static string ErrorCodeImpossibleEstablishmentYear => "impossible_establishment_year";
    }
}
