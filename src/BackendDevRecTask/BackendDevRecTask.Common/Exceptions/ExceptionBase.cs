﻿namespace BackendDevRecTask.Common.Exceptions
{
    using System;

    public abstract class ExceptionBase : Exception
    {
        public string Code { get; }

        protected ExceptionBase()
        {
        }

        public ExceptionBase(string code)
        {
            Code = code;
        }

        public ExceptionBase(string message, params object[] args) : this(string.Empty, message, args)
        {
        }

        public ExceptionBase(string code, string message, params object[] args) : this(null, code, message, args)
        {
        }

        public ExceptionBase(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public ExceptionBase(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}
