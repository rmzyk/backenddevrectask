﻿namespace BackendDevRecTask.Tests.ControllerTests
{
    using System.Net.Http;
    using System.Text;
    using Api;
    using Microsoft.AspNetCore.Hosting;
    using Newtonsoft.Json;
    using Microsoft.AspNetCore.TestHost;

    public abstract class ControllerTestBase
    {
        protected readonly TestServer Server;
        protected readonly HttpClient Client;

        protected ControllerTestBase()
        {
            Server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            Client = Server.CreateClient();
        }

        protected static StringContent GetPayload(object data)
        {
            var json = JsonConvert.SerializeObject(data);

            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}
