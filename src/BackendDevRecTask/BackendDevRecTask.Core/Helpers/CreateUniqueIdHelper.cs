﻿namespace BackendDevRecTask.Core.Helpers
{
    using System;

    public static class CreateUniqueIdHelper
    {
        public static long GetSimpleTimeStampId()
        {
            var epoch = new DateTime(1970,1,1,0,0,0, DateTimeKind.Utc);
            var time = DateTime.UtcNow.Subtract(new TimeSpan(epoch.Ticks));
            return time.Ticks;
        }
    }
}
