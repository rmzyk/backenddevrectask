﻿namespace BackendDevRecTask.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using Helpers;

    public class Company
    {
       public Company(string name,
           short establishmentYear, IEnumerable<Employee> employees)
       {
            this.CompanyId = Guid.NewGuid();
            this.SetName(name);
            this.SetEstablishmentYear(establishmentYear);
            this.SetEmployees(employees);
       }

       protected Company()
       {
       }


       public Guid CompanyId { get; protected set; }

       public string Name { get; protected set; }

       public short EstablishmentYear { get; protected set; }

       public IEnumerable<Employee> Employees { get; protected set; }


       public void UpdateFrom(Company company)
       {
           this.SetName(company.Name);
           this.SetEstablishmentYear(company.EstablishmentYear);
       }

       public void SetName(string name)
            =>this.Name = name;

       public void SetEstablishmentYear(short establishmentYear)
            => this.EstablishmentYear = establishmentYear;

       public void SetEmployees(IEnumerable<Employee> employees)
            => this.Employees = employees;
    }
}
