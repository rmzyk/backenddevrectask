﻿namespace BackendDevRecTask.Core.Domain
{
    using System;
    using Helpers;
    using Utils;

    public class Employee
    {
        public Employee(string firstName, string lastName, DateTime dateOfBirth, JobTitle jobTitle)
        {
            EmployeeId = Guid.NewGuid();
            this.SetFirstName(firstName);
            this.SetLastName(lastName);
            this.SetDateOfBirth(dateOfBirth);
            this.SetJobTitle(jobTitle);
        }

        public Guid EmployeeId { get; protected set; }

        public Company Company { get; protected set; }

        public string FirstName { get; protected set; }

        public string LastName { get; protected set; }

        public DateTime DateOfBirth { get; protected set; }

        public JobTitle JobTitle { get; protected set; }

        public void SetCompany(Company company)
        {
            this.Company = company;
        }

        public void SetFirstName(string firstName)
        {
            this.FirstName = firstName;
        }

        public void SetLastName(string lastName)
        {
            this.LastName = lastName;
        }

        public void SetDateOfBirth(DateTime dateOfBirth)
        {
            this.DateOfBirth = dateOfBirth;
        }

        public void SetJobTitle(JobTitle jobTitle)
        {
            this.JobTitle = jobTitle;
        }
    }
}
