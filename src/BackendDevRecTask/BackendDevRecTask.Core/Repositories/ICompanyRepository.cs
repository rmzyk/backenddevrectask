﻿namespace BackendDevRecTask.Core.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Domain;

    public interface ICompanyRepository : IRepository
    {
        Task<IEnumerable<Company>> GetByFilterAsync();

        Task UpdateAsync(Company company);

        Task DeleteAsync(Company company);

        Task CreateAsync(Company company);

        Task<Company> GetByNameAsync(string name);

        Task<Company> GetByIdAsync(Guid id);
    }
}
