﻿using System;

namespace BackendDevRecTask.Core.Utils
{
    [Flags]
    public enum JobTitle
    {
        // Description Header
        // Job Titles defined for Employees.

        Administrator = 0,
        Architect = 1,
        Developer = 2,
        Manager = 3
    }
}
