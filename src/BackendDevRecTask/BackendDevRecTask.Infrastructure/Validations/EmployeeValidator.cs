﻿namespace BackendDevRecTask.Infrastructure.Validations
{
    using Core.Domain;
    using API.IValidators;
    using FluentValidation;
    using System;
    using System.IO;
    using Common.Exceptions;

    public class EmployeeValidator : AbstractValidator<Company>, IEmployeeValidator
    {
        private readonly int minimumNameLength = 4;
        private readonly int maximumNameLength = 50;

        public EmployeeValidator()
        {
            RuleFor(x => x.Name)
                .MinimumLength(minimumNameLength)
                .MaximumLength(maximumNameLength)
                .OnAnyFailure((x) => throw new InvalidDataException())
                .WithErrorCode(ErrorCodes.ErrorCodeInvalidCompanyNameFormat);

            RuleFor(x => x.EstablishmentYear)
                .LessThan((Int16)DateTime.UtcNow.Year)
                .OnAnyFailure((x) => throw new InvalidDataException())
                .WithErrorCode(ErrorCodes.ErrorCodeImpossibleEstablishmentYear);
        }
    }
}
