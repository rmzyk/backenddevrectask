﻿namespace BackendDevRecTask.Infrastructure.IoC.Modules
{
    using System.Reflection;
    using Autofac;
    using Core.Repositories;
    using BackendDevRecTask.DataMsSql.API;
    using DataMsSql.EntityFrameworkCore;
    using API.IRepositories;

    public class MsSqlRepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var assembly = typeof(BackendDevRecTaskContext)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IRepository>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IMsSqlRepository>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IDatabaseSeed>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}