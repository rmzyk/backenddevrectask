﻿namespace BackendDevRecTask.Infrastructure.IoC.Modules
{
    using Autofac;
    using Common.Extensions;
    using Microsoft.Extensions.Configuration;
    using BackendDevRecTask.DataMsSql.EntityFrameworkCore;

    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterInstance(_configuration.GetSettings<MsSqlSettings>());
        }
    }
}
