﻿namespace BackendDevRecTask.Infrastructure.IoC.Modules
{
    using Autofac;
    using Mappers;
    using Microsoft.Extensions.Configuration;

    public class ContainerModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterInstance(AutomapperConfig.Initialize()).SingleInstance();
            builder.RegisterModule(new SettingsModule(_configuration));
            builder.RegisterModule<CommandModule>();
            builder.RegisterModule<MsSqlRepositoryModule>();
            builder.RegisterModule<ServiceModule>();
        }
    }
}
