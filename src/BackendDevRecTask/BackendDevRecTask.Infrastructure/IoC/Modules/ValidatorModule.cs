﻿namespace BackendDevRecTask.Infrastructure.IoC.Modules
{
    using System.Reflection;
    using Autofac;
    using API.IValidators;

    public class ValidatorModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var assembly = typeof(ValidatorModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IValidator>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
