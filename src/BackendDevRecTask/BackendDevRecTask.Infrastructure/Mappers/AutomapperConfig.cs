﻿namespace BackendDevRecTask.Infrastructure.Mappers
{
    using AutoMapper;
    using BackendDevRecTask.DataMsSql.DAO;
    using Commands.Company;
    using Core.Domain;
    using DTO;

    public static class AutomapperConfig
    {
        public static IMapper Initialize()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Company, CompanyDto>();
                cfg.CreateMap<CompanyDto, Company>();
                cfg.CreateMap<Employee, EmployeeDto>();
                cfg.CreateMap<EmployeeDto, Employee>();
                cfg.CreateMap<CreateCompany, CompanyDto>();
                cfg.CreateMap<UpdateCompany, Company>();
                cfg.CreateMap<CompanyEntity, Company>();
                cfg.CreateMap<EmployeeEntity, Employee>();
                cfg.CreateMap<Employee, EmployeeEntity>();
                cfg.CreateMap<Company, CompanyEntity>();
            })
                .CreateMapper();
    }
}
