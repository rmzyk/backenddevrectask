﻿namespace BackendDevRecTask.Infrastructure.API.ICommands
{
    using System.Threading.Tasks;

    public interface ICommandHandler<T> where T : ICommand
    {
        Task HandleAsync(T command);
    }
}
