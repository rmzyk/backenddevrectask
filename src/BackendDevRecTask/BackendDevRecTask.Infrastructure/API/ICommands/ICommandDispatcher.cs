﻿namespace BackendDevRecTask.Infrastructure.API.ICommands
{
    using System.Threading.Tasks;

    public interface ICommandDispatcher
    {
        Task DispatchAsync<T>(T command) where T : ICommand;
    }
}
