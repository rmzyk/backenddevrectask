﻿namespace BackendDevRecTask.Infrastructure.API.IServices
{
    using Core.Domain;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DTO;
    using System;

    public interface ICompanyService : IService
    {
        Task<Guid> CreateAsync(CompanyDto companyDto);

        Task UpdateAsync(Company company);

        Task DeleteAsync(Guid id);

        Task<IEnumerable<CompanyDto>> GetByFilterAsync(BasicFilterDto filterDto);

        Task<CompanyDto> GetByNameAsync(string name);

        Task<CompanyDto> GetByIdAsync(Guid id);
    }
}
