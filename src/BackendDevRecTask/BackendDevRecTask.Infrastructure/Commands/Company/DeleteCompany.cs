﻿namespace BackendDevRecTask.Infrastructure.Commands.Company
{
    using API.ICommands;
    using System;

    public class DeleteCompany : ICommand
    {
        public Guid CompanyId { get; set; }
    }
}
