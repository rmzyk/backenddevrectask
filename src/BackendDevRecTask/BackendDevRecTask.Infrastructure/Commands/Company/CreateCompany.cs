﻿namespace BackendDevRecTask.Infrastructure.Commands.Company
{
    using DTO;
    using System.Collections.Generic;
    using API.ICommands;
    using System;

    public class CreateCompany : ICommand
    {
        public Guid CompanyId { get; set; }

        public string Name { get; set; }

        public short EstablishmentYear { get; set; }

        public IEnumerable<EmployeeDto> Employees {get; set; } = new List<EmployeeDto>();
    }
}
