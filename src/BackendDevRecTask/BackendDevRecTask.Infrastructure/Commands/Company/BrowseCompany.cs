﻿using System;
using System.Collections.Generic;
using BackendDevRecTask.Core.Utils;

namespace BackendDevRecTask.Infrastructure.Commands.Company
{
    using API.ICommands;

    public class BrowseCompany : ICommand
    {
        public string Keyword { get; set; }

        public DateTime EmployeeDateOfBirthFrom { get; set; }

        public DateTime EmployeeDateOfBirthTo { get; set; }
        
        public IEnumerable<JobTitle> JobTitles { get; set; }
    }
}
