﻿namespace BackendDevRecTask.Infrastructure.Commands.Company
{
    using System.Collections.Generic;
    using DTO;
    using API.ICommands;
    using System.ComponentModel.DataAnnotations;

    public class UpdateCompany : ICommand
    {
        [Required]
        public long CompanyId { get; set; }

        [Required, MaxLength(40)]
        public string Name { get; set; }

        [Required]
        public short EstablishmentYear { get; set; }

        [Required]
        public IEnumerable<EmployeeDto> Employees { get; set; } 
    }
}
