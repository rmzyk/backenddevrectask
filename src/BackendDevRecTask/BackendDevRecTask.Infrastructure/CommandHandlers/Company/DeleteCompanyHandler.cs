﻿namespace BackendDevRecTask.Infrastructure.CommandHandlers.Company
{
    using System.Threading.Tasks;
    using API.ICommands;
    using Commands.Company;
    using API.IServices;

    public class DeleteCompanyHandler : ICommandHandler<DeleteCompany>
    {
        private readonly ICompanyService _companyService;

        public DeleteCompanyHandler(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public Task HandleAsync(DeleteCompany command)
            => _companyService.DeleteAsync(command.CompanyId);
    }
}
