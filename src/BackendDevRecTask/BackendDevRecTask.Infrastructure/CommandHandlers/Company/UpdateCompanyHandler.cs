﻿namespace BackendDevRecTask.Infrastructure.CommandHandlers.Company
{
    using System.Threading.Tasks;
    using API.ICommands;
    using API.IServices;
    using AutoMapper;
    using Commands.Company;

    public class UpdateCompanyHandler : ICommandHandler<UpdateCompany>
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;

        public UpdateCompanyHandler(ICompanyService companyService,
            IMapper mapper)
        {
            _companyService = companyService;
            _mapper = mapper;
        }

        public async Task HandleAsync(UpdateCompany command)
            => await _companyService.UpdateAsync(_mapper.Map<Core.Domain.Company>(command));
    }
}
