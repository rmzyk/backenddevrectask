﻿namespace BackendDevRecTask.Infrastructure.CommandHandlers.Company
{
    using System.Threading.Tasks;
    using API.ICommands;
    using API.IServices;
    using Commands.Company;
    using AutoMapper;
    using DTO;

    public class CreateCompanyHandler : ICommandHandler<CreateCompany>
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;

        public CreateCompanyHandler(ICompanyService companyService,
            IMapper mapper)
        {
            _companyService = companyService;
            _mapper = mapper;
        }

        public async Task HandleAsync(CreateCompany command)
        {
          var id =  await _companyService.CreateAsync(_mapper.Map<CompanyDto>(command));
          command.CompanyId = id;
        }
    }
}

