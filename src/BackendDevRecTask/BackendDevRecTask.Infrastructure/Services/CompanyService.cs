﻿namespace BackendDevRecTask.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using API.IServices;
    using DTO;
    using AutoMapper;
    using Core.Domain;
    using Core.Repositories;
    using Common.Exceptions;

    public class CompanyService : ICompanyService
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository companyRepository,
            IMapper mapper)
        {
            _companyRepository = companyRepository;
            _mapper = mapper;
        }

        public async Task<Guid> CreateAsync(CompanyDto companyDto)
        {
            var dbCompany = await _companyRepository.GetByNameAsync(companyDto.Name);
            if (dbCompany != null)
            {
                throw new ServiceException(ErrorCodes.ErrorCodeCompanyNameInUse);
            }

            try
            {
                var company = _mapper.Map<Company>(companyDto);
                await _companyRepository.CreateAsync(company);

                return company.CompanyId;
            }
            catch (Exception ex)
            {
                //TODO:
            }

            return Guid.Empty; //TODO
        }

        public async Task UpdateAsync(Company company)
        {
            if (company.Name == String.Empty || company.Employees is null)
            {
                throw new ServiceException(ErrorCodes.ErrorCodeIncompleteDataProvided);
            }

            var dbCompany = await _companyRepository.GetByIdAsync(company.CompanyId);
            if (dbCompany is null)
            {
               throw new ServiceException(ErrorCodes.ErrorCodeCompanyNotFound);
            }

            dbCompany.UpdateFrom(company);

            try
            {
                await _companyRepository.UpdateAsync(company);
            }
            catch (Exception ex)
            {
                // TODO
            }
        }

        public async Task DeleteAsync(Guid companyId)
        {
            var dbCompany = await _companyRepository.GetByIdAsync(companyId);

            if (dbCompany is null)
            {
                throw new ServiceException(ErrorCodes.ErrorCodeCompanyNotFound);
            }

            try
            {
                await _companyRepository.DeleteAsync(dbCompany);
            }
            catch (Exception exception)
            {
                // TODO
            }
        }

        public async Task<IEnumerable<CompanyDto>> GetByFilterAsync(BasicFilterDto filterDto)
        {
            throw new NotImplementedException();
        }

        public async Task<CompanyDto> GetByNameAsync(string name)
        {
            var dbCompany = await _companyRepository.GetByNameAsync(name);
            return _mapper.Map<CompanyDto>(dbCompany);
        }

        public async Task<CompanyDto> GetByIdAsync(Guid id)
        {
            var dbCompany = await _companyRepository.GetByIdAsync(id);
            var resultDto = _mapper.Map<CompanyDto>(dbCompany);
            return resultDto;
        }
    }
}
