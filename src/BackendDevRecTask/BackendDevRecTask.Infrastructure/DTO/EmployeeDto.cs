﻿namespace BackendDevRecTask.Infrastructure.DTO
{
    using System;
    using Core.Utils;
    using System.ComponentModel.DataAnnotations;

    public class EmployeeDto
    {
        [Required, MaxLength(50)]
        public string FirstName { get; set; }

        [Required, MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public JobTitle JobTitle { get; set; }
    }
}
