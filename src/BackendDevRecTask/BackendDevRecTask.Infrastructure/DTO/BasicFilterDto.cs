﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BackendDevRecTask.Core.Utils;

namespace BackendDevRecTask.Infrastructure.DTO
{
    public class BasicFilterDto
    {
        
        public string Keyword { get; set; }

        public DateTime? EmployeeDateOfBirthFrom { get; set; }

        public DateTime? EmployeeDateOfBirthTo { get; set; }

        public IEnumerable<JobTitle> EmployeeJobTitles { get; set; } = new List<JobTitle>();
    }
}
