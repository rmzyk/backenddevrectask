﻿using System.ComponentModel.DataAnnotations;

namespace BackendDevRecTask.Infrastructure.DTO
{
    using System.Collections.Generic;

    public class CompanyDto
    {
        [Required]
        [MaxLength(50)]
        public string Name { get;  set; }

        [Required]
        [MaxLength(4)]
        public short EstablishmentYear { get;  set; }

        [Required]
        public IEnumerable<EmployeeDto> Employees { get;  set; } 
    }
}
